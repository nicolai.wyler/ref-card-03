# Architecture Ref. Card 03

## Service Adresse

http://lbm169-1761176663.us-east-1.elb.amazonaws.com/

## Voraussetzungen

1. AWS zugang zu ECR,ECS und EC2
2. Gitlab Account
3. Gitlab Runner

### Hochladen des Images 

1. Aus dem pom.xml die Java Version auslesen.
2. .Dockerfile mit der angepassten Java Version erstellen
3. Repository in AWS erstellen.
4. Gitlab Variabeln Setzen 
Learnerlab: 
AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, AWS_SESSION_TOKEN
Repository:
AWS_DEFAULT_REGION, CI_AWS_ECR_REGISTRY, CI_AWS_ECR_REPOSITORY_NAME 
5. Erstellen des .gitlab-ci.yml File

### Erstellen des ECS Cluster

1. Namen des Clusters angeben 
2. Erstellen des Clusters

### Erstellen der Task Definition 

1. Namen angeben
2. Image Url aus dem Repository kopieren und angeben
3. Task-Rolle, Task-Ausführungsrolle zu LabRole konfigurieren 

### Erstellen des Services

1. Launchtype Service auswählen 
2. Application Type Service auswählen 
3. Die zuvor erstellte Family auswählen 
4. Den Service Namen angeben
5. Service Type Replica auswählen

Beim erstellen der Services kann eine neue Security Group erstellt werden dafür muss unter Netzwerke eine erstellt werden.
In meinem Fall ist diese so konfiguriert das der komplette Traffic zugelassen wird.

### Loadbalancing im Service konfigurieren

1. Application Loadbalancer auswählen 
2. Create new Loadbalancer auswählen 
3. Loadbalancer Name festlegen 
4. Health Check grace period angeben 

### Testen der Applikation

1. Zu EC2 in AWS wechseln 
2. Unter dem Punkt Loadbalancer den DNS Namen Kopieren und im Browser aufrufen

# Fehlerbehebung

### Image wird nicht ins Repository hochgeladen

1. Um den Status des Jobs zu kontrollieren kann man im Fork auf der rechten Seite unter Build und Jobs den Status überprüfen.




